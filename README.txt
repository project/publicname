This module allows users to have a public name, shown as attribution and identity but not used for login.
There are some significant limitations you must know about if you choose to use this module.

You need to be using the email_registration and content_profile modules.
Will need to set up a CCK Content Profile type and configure it appropriately.
You will also want to assign appropriate permissions so that the appropriate users can create/edit their own content for the type of node used by this module.

The title of the configured Content Profile type (see below) is linked to the user's 'name' attribute.
The understanding is that you have chosen to use the content profile module for the same reasons we choose to use it. :-)

The strategy of this module requires using the email address to authenticate the user, instead of the user name attribute.
This functionality is mostly provided by the email_registration module.

The user->name attribute is disabled for login use.  Otherwise it works exactly the same.  
This means that all core and contrib functionality attributing, linking, listing users continues working the same way.

If you turn off the module after it has been in use, it should not hurt your system, 
but it will mean that users can log in with the 'public name' advertised as their identity on your site.

CONFIGURATION
Enable email_registration and content_profile modules.  Use the Profile content type or create/configure your own, 
to be the user's profile, the title of which will store the user's name.

The title of the configured Content Profile type (see below) is linked to the user's 'name' attribute.

In your content profile content type definition, on the content profile tab, you will find a checkbox that 
will set this content type to be the user name type.  To avoid a highly interesting situation, 
you should only set this value to true for one content type!